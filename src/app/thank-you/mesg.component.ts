import { Component, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Router } from "@angular/router";


@Component({
    selector: 'app-mesg',
    templateUrl: './mesg.component.html',
    styleUrls: ['./mesg.component.scss']
})
export class MesgComponent {
    clothes: any = [];
    dressNames: any = [];
    dialogRef: any;
    cartCount: any;

    constructor(public dialog: MatDialog, private router: Router) { }

    ngOnInit(): void {
        this.cartCount = localStorage.getItem('cartCount');
        for (let i = 0; i < this.cartCount; i++) {
            localStorage.removeItem('dress' + (i + 1).toString());
        }
        localStorage.setItem('cartCount', '0');
        
    }

}