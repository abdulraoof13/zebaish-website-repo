import { Component, OnInit } from "@angular/core";
import { DressService } from "./dress.services";
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from "../categories/categories.services";
@Component({
  selector: 'app-dress',
  templateUrl: './dress.component.html',
  styleUrls: ['./dress.component.scss']
})
export class DressComponent implements OnInit {
  displayPic: any;
  isLoading: String = 'd-none';
  dress: any;
  reviews: any = [];
  empty: any = '';
  reviewAverage: any = 0;
  days:any;
  show: any = 'd-none';
  message: any = '';

  constructor(private dressService: DressService, private route: ActivatedRoute, private categoryService:CategoryService) { }

  ngOnInit(): void {
    this.reviewAverage = 0;
    this.isLoading = 'loading'
    let id = this.route.snapshot.paramMap.get('id');
    console.log(id);
    this.dressService.getDress(id);
    this.dressService.gotDressDetails.subscribe((response: any) => {
      if (response._id == id) {
        this.dress = response;
        debugger;
        this.displayPic = this.dress.pic_urls[0];
        this.isLoading = 'd-none';
      }

    });

    this.categoryService.addedToCart.subscribe((response: any) => {
      if (response == true) {
        this.show = '';
        this.message = 'Dress added to cart';
        let that = this;
        setTimeout(function () {
          that.show = 'd-none';
        }, 6000)
      }
    });

    this.dressService.gotReview.subscribe((response: any) => {
      this.reviewAverage = 0;
      if (response.length > 0) {
        debugger;
        this.reviews = response;
        let sum = 0;
        for (let i = 0; i < this.reviews.length; i++) {
          sum = sum + this.reviews[i].rating;
        }
        this.reviewAverage = sum / this.reviews.length;
        this.empty = '';
      }
      else if (response.length == 0) {
        this.empty = 'empty';
      }
    });
  }

  addToCart(obj:any) {
    if (this.days == '') {
      //this.warning = true;
      return;
    }
    obj.orderedDays = this.days;
    let cart = localStorage.getItem('cartCount')
    let cartCount = parseInt(cart || '') + 1;
    localStorage.setItem('cartCount', cartCount.toString());
    localStorage.setItem('dress' + cartCount.toString(), JSON.stringify(obj));
    this.categoryService.addedToCart.next(true);
  }

  changeDisplayPic(image: any) {
    this.displayPic = image;
  }

}