import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, Subject } from "rxjs";
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';
import { of } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from "@angular/core";
import { AuthService } from '../auth/auth.service'

@Injectable({ providedIn: 'root' })
export class DressService implements Resolve<any> {
    apiUrl: any = environment.apiUrl;
    gotDressDetails: BehaviorSubject<any> = new BehaviorSubject({});
    gotReview: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private router: Router, private authService: AuthService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        return new Promise((resolve, reject) => {

            Promise.all([
                //this.getDress()
            ]).then(
                ([classes]) => {
                    resolve(Response);
                },
                reject
            );
        });
    }

    getDress(id:any): Promise<any> {
        let token = localStorage.getItem('token')
        return new Promise((resolve, reject) => {
            
            this.http.get(`${this.apiUrl}/api/clothes/${id}`).subscribe((response:any)=>{
                debugger;
                if(response.cloth.pic_urls.length>0) {
                  this.gotDressDetails.next(response.cloth);
                  this.gotReview.next(response.reviews);
                }
                              
            });
        });

    }
}