import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { CategoriesComponent } from './categories/categories.component';
import { DressComponent } from './dress/dress.composnent';
import { CartComponent } from './cart/cart.component';
import { OrderComponent } from './order/order.component';
import { ProfileSettingComponent } from './profile-setting/profile-setting.component';
import { MesgComponent } from './thank-you/mesg.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-passowrd.component';
import { HelpComponent } from './help/help.component';
import { SearchComponent } from './search/search.component';


const routes: Routes = [
  {path: '', component: MainComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'login', component: LoginComponent},
  {path: 'categories/:id', component: CategoriesComponent},
  {path: 'dress/:id', component:DressComponent},
  {path: 'cart', component:CartComponent},
  {path: 'order', component:OrderComponent},
  {path: 'profile', component:ProfileSettingComponent},
  {path: 'thankyou', component:MesgComponent},
  {path: 'forgot', component:ForgotPasswordComponent},
  {path: 'help/:id', component:HelpComponent},
  {path: 'search', component:SearchComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
