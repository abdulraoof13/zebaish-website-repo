import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, Subject } from "rxjs";
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';
import { of } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from "@angular/core";
import { AuthService } from '../auth/auth.service';

@Injectable({ providedIn: 'root' })
export class ForgotService implements Resolve<any> {
    apiUrl: any = environment.apiUrl;
    gotOtp: BehaviorSubject<any> = new BehaviorSubject({});
    changePassword: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private router: Router, private authService: AuthService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        return new Promise((resolve, reject) => {

            Promise.all([
                //this.getDress()
            ]).then(
                ([classes]) => {
                    resolve(Response);
                },
                reject
            );
        });
    }

    sendOtp(email: any): Promise<any> {

        return new Promise((resolve, reject) => {

            this.http.get(`${this.apiUrl}/api/user/getOTP?email=${email}`).subscribe((response: any) => {
                debugger;
                if (response.message == "OTP successfully sent by email.") {
                    this.gotOtp.next(true);
                }
                else if (response.message == "user not found for the given email."){
                    this.gotOtp.next(false);
                }
            });
        });
    }

    checkOtp(otp: any,pass:any,email:any): Promise<any> {

        return new Promise((resolve, reject) => {

            this.http.post(`${this.apiUrl}/api/user/checkOTP`,{otp:otp,password:pass,email:email}).subscribe((response: any) => {
                debugger;
                if (response.message == "Update successful!") {
                    this.changePassword.next(true);
                }
                else if (response.message != "Update successful!"){
                    this.changePassword.next(false);
                }
            });
        });
    }


}