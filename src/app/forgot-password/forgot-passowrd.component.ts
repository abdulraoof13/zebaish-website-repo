import { Component } from "@angular/core";
import { AuthService } from "../auth/auth.service";
import { ForgotService } from "./forgot-password.service";
import { Router } from "@angular/router"; 
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {

  email: any = '';
  newPassword: any = '';
  otp: any;
  isLoading: String = '';
  message: any = "";
  show: any = "d-none";
  sent: Boolean = false;

  constructor(private authService: AuthService, private forgotService: ForgotService, private router:Router) { }

  getOtp() {
    if (this.email != '') {
      this.forgotService.sendOtp(this.email);
      this.forgotService.gotOtp.subscribe((response: any) => {
        if (response == true) {
          this.sent = true;
        }
        else if (response == false) {
          this.show = '';
          this.message = 'Account with this email does not exist';
          let that = this;
          setTimeout(function () {
            that.show = 'd-none';
          }, 6000)
        }
      });
    }
    else {
      this.show = '';
      this.message = 'Please enter email';
      let that = this;
      setTimeout(function () {
        that.show = 'd-none';
      }, 6000)
    }
  }

  resetPassword() {
    if (this.otp == '') {
      this.show = '';
      this.message = 'Please enter otp';
      let that = this;
      setTimeout(function () {
        that.show = 'd-none';
      }, 6000)
    }
    else if (this.newPassword == '') {
      this.show = '';
      this.message = 'Please enter new password';
      let that = this;
      setTimeout(function () {
        that.show = 'd-none';
      }, 6000)
    }
    else {
      this.forgotService.checkOtp(this.otp, this.newPassword, this.email);
      this.forgotService.changePassword.subscribe((response: any) => {
        if (response == true) {
          this.show = '';
          this.message = 'Password changed!';
          let that = this;
          setTimeout(function () {
            that.show = 'd-none';
            that.router.navigate(['./login']);
          }, 3000)
        }
        else if(response == false) {
          this.show = '';
          this.message = 'Something went wrong! make sure you enter the correct OTP';
          let that = this;
          setTimeout(function () {
            that.show = 'd-none';
          }, 3000)
        }
      });
    }


  }

}