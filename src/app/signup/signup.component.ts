import { Component, ViewEncapsulation, ElementRef, Input, OnInit, OnDestroy, Inject } from "@angular/core";
import { FormBuilder, FormControl, Validators, FormGroup, NgForm, NgModel } from '@angular/forms';
import { AuthService } from '../auth/auth.service';
import { Router } from "@angular/router";

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

    params: any = {
        email: '',
        password: '',
        first_name: '',
        last_name: '',
        gender: 0,
        role: 2,
        mobile: '',
        cnic: '',
        city: 1,
        country: 1,
        address: ''
    }
    cPassword: any;
    message: any = "";
    show: any = "d-none";
    isLoading: String = 'd-none';
    agree: Boolean = false;

    constructor(private auth: AuthService, private router: Router) { }

    ngOnInit(): void {

    }

    signup() {
        debugger;
        this.isLoading = 'loading'
        if (this.params.email == "" || this.params.password == "" || this.params.first_name == "" || this.params.last_name == "" || this.params.gender == 0 || this.params.mobile == "" || this.params.cnic == "" || this.params.city == 0 || this.params.country == 0 || this.params.address == "") {
            this.message = "Please Fill All Fields";
            this.show = '';
            this.isLoading = 'd-none';
            return;
        }
        else if (this.params.password != this.cPassword) {
            this.message = "Passwords Do Not Match";
            this.show = '';
            this.isLoading = 'd-none';
            return;
        }
        else if (this.agree == false) {
            this.message = "Please agree to the terms and conditions, we cannot proceed without it.";
            this.show = '';
            this.isLoading = 'd-none';
            return;
        }


        this.auth.createUser(this.params);
        this.auth.onSignup.subscribe((response: any) => {
            debugger;
            if (response == false) {
                this.show = '';
                this.message = 'An Account Already exists with same email, phone no. or CNIC';
                let that = this;
                setTimeout(function () {
                    that.show = 'd-none';
                }, 6000)
            }
            else if (response == true) {
                this.isLoading = "d-none";
                this.show = '';
                this.message = 'Sign Up successfull! now login with your account credentials';
                let that = this;
                setTimeout(function () {
                    that.show = 'd-none';
                    that.router.navigate(['/login']);
                }, 6000)
                
            }
        })



        console.log(this.params);
    }

}