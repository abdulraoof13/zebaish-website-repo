import { Component, Inject, ViewEncapsulation, AfterViewChecked } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
    selector: 'proceed-dialog',
    templateUrl: './proceed.component.html',
    styleUrls: ['./proceed.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ProceedComponent {

    days:any='';
    warning:Boolean = false;
    radioSelected:any

    constructor(public dialogRef: MatDialogRef<ProceedComponent>, @Inject(MAT_DIALOG_DATA) public data: any,private router:Router) {
        this.data;
    }

    ngOnInit()
    {
        
        console.log(this.data);

    }

    addToCart(obj:any) {
        if(this.days=='') {
            this.warning=true;
            return;
        }
        obj.orderedDays = this.days;
        let cart = localStorage.getItem('cartCount')
        let cartCount = parseInt(cart||'') + 1;
        localStorage.setItem('cartCount',cartCount.toString());
        localStorage.setItem('dress'+cartCount.toString(),JSON.stringify(obj));
    }

    closeDialog(action:any) {
        if(action=='cancel'){
            this.dialogRef.close();
        }   
    }

    checkOut() {
        console.log(this.radioSelected);
        localStorage.setItem('toOrder',JSON.stringify(this.radioSelected));
        this.router.navigate(['/order']);
        this.closeDialog('cancel');
    }

}