import { Component, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ProceedComponent } from "./proceed/proceed.component";
import { Router } from "@angular/router";


@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss']
})
export class CartComponent {
    clothes: any = [];
    cartCount: any;
    dressNames: any = [];
    dialogRef: any;

    constructor(public dialog: MatDialog, private router: Router) { }

    ngOnInit(): void {

        this.cartCount = localStorage.getItem('cartCount');
        for (let i = 0; i < parseInt(this.cartCount || '0'); i++) {
            this.clothes.push(JSON.parse(localStorage.getItem('dress' + (i + 1).toString()) || '{}'))
            this.dressNames.push('dress' + (i + 1).toString());
        }
        console.log(this.clothes);
    }

    deleteDress(id: any) {
        this.clothes.splice(id, 1)
        for (let i = 0; i < this.cartCount; i++) {
            localStorage.removeItem('dress' + (i + 1).toString());
        }
        console.log(this.clothes);
        let cart = parseInt(this.cartCount) - 1
        this.cartCount = cart;
        localStorage.setItem('cartCount', cart.toString());
        for (let i = 0; i < cart; i++) {
            localStorage.setItem('dress' + (i + 1).toString(), JSON.stringify(this.clothes[i]));
        }

    }

    proceedToCheckout() {
        // if (this.cartCount > 1) {
        //     this.openDialog(1);
        // }
        // else if (this.cartCount == 1) {
        //     let dress = this.clothes[0]
        //     localStorage.setItem('toOrder', JSON.stringify(dress));
            this.router.navigate(['/order']);
        //}
    }

    openDialog(event: any) {
        this.dialogRef = this.dialog.open(ProceedComponent, {
            panelClass: 'event-form-dialog',
            data: {
                dressData: this.clothes
            }
        });
        this.dialogRef.afterClosed().subscribe((response: any) => {

        });
    }
}