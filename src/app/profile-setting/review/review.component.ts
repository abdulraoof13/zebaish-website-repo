import { Component, Inject, ViewEncapsulation, AfterViewChecked } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ProfileService } from '../profile.service'

@Component({
    selector: 'review-dialog',
    templateUrl: './review.component.html',
    styleUrls: ['./review.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ReviewComponent {

    days:any='';
    warning:Boolean = false;
    radioSelected:any;
    comment:any;

    constructor(public dialogRef: MatDialogRef<ReviewComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private profileService: ProfileService) {
        this.data;
    }

    ngOnInit()
    {
        
        console.log(this.data);

    }

    rating(num:any) {

    }

    sendReview() {
        debugger;
        let review={
            remarks: this.comment,
  			rating: parseInt(this.radioSelected),
 			cloth_id: this.data.dressData.cloth_id,
  			customer_id: this.data.dressData.customer_id,
            order_id: this.data.dressData._id
        };

        console.log(review);

        this.profileService.sendReview(review)
        this.closeDialog('cancel');
    }

    addToCart(obj:any) {
        if(this.days=='') {
            this.warning=true;
            return;
        }
        obj.orderedDays = this.days;
        let cart = localStorage.getItem('cartCount')
        let cartCount = parseInt(cart||'') + 1;
        localStorage.setItem('cartCount',cartCount.toString());
        localStorage.setItem('dress'+cartCount.toString(),JSON.stringify(obj));
    }

    closeDialog(action:any) {
        if(action=='cancel'){
            this.dialogRef.close();
        }
    }

    

}