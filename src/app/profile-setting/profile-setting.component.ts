import { Component, OnInit } from "@angular/core";
import { AuthService } from '../auth/auth.service';
import { ProfileService } from "./profile.service";
import { ReviewComponent } from "./review/review.component";
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';

@Component({
    selector: 'app-profile-setting',
    templateUrl: './profile-setting.component.html',
    styleUrls: ['./profile-setting.component.scss']
})
export class ProfileSettingComponent {
    clothes: any;
    showDetails: any = "";
    showCurrentOrders: any = "d-none";
    showPastOrders: any = "d-none";
    showChangePassword: any = "d-none";
    editPro: any = 'd-none';
    displayPro: any = '';
    name: any;
    orders: any;
    isLoading: any = false;
    dialogRef: any
    oldPassword: any;
    newPassword: any;
    confirmPassword: any;
    show: any = 'd-none';
    message: any = '';
    user:any;
    activated1:any='';
    activated2:any='';
    activated3:any='';
    activated4:any='';

    params: any = {
        email: '',
        password: '',
        first_name: '',
        last_name: '',
        gender: 0,
        role: 2,
        phone: '',
        cnic: '',
        city: 1,
        country: 1,
        address: ''
    }

    constructor(private authService: AuthService, private profileService: ProfileService, public dialog: MatDialog) { }

    ngOnInit(): void {
        this.user = this.authService.getUserObj();
        this.name = this.user.first_name;
        this.params = {
            _id: this.user._id,
            email: this.user.email,
            password: this.user.password,
            first_name: this.user.first_name,
            last_name: this.user.last_name,
            gender: this.user.gender,
            role: 2,
            phone: this.user.phone,
            cnic: this.user.cnic,
            city: this.user.city,
            country: this.user.country,
            address: this.user.address
        }
        this.activated1 = 'activated';
    }

    showWindow(num: Number) {
        if (num == 1) {
            this.activated1 = 'activated';
            this.activated2 = '';
            this.activated3 = '';
            this.activated4 = '';
            this.showDetails = "";
            this.showCurrentOrders = "d-none";
            this.showPastOrders = "d-none";
            this.showChangePassword = "d-none";
        }
        else if (num == 2) {
            this.activated1 = '';
            this.activated2 = 'activated';
            this.activated3 = '';
            this.activated4 = '';
            this.showDetails = "d-none";
            this.showCurrentOrders = "";
            this.showPastOrders = "d-none";
            this.showChangePassword = "d-none";
            this.isLoading = true;
            this.profileService.getOrders();
            this.profileService.gotOrder.subscribe((response: any) => {
                if (response.orders) {
                    this.isLoading = false;
                }
                if (response.orders && response.orders.length > 0) {
                    this.orders = response.orders;
                }
            });
        }
        else if (num == 3) {
            this.activated1 = '';
            this.activated2 = '';
            this.activated3 = 'activated';
            this.activated4 = '';
            this.showDetails = "d-none";
            this.showCurrentOrders = "d-none";
            this.showPastOrders = "";
            this.showChangePassword = "d-none";
            this.isLoading = true;
            this.profileService.getOrders();
            this.profileService.gotOrder.subscribe((response: any) => {
                if (response.orders) {
                    this.isLoading = false;
                }
                if (response.orders && response.orders.length > 0) {
                    this.orders = response.orders;
                }
            });
        }
        else if (num == 4) {
            this.activated1 = '';
            this.activated2 = '';
            this.activated3 = '';
            this.activated4 = 'activated';
            this.showDetails = "d-none";
            this.showCurrentOrders = "d-none";
            this.showPastOrders = "d-none";
            this.showChangePassword = "";
        }
    }

    edit() {
        this.editPro = '';
        this.displayPro = 'd-none';
    }

    openDialog(event: any) {
        this.dialogRef = this.dialog.open(ReviewComponent, {
            panelClass: 'event-form-dialog',
            data: {
                dressData: event
            }
        });
        this.dialogRef.afterClosed().subscribe((response: any) => {
            this.profileService.reviewSent.subscribe((response: any) => {
                if (response == true) {
                    this.show = '';
                    this.message = 'Thanks For your Review';
                    let that = this;
                    this.profileService.getOrders();
                    setTimeout(function () {
                        that.show = 'd-none';
                    }, 6000)
                }
            });
        });
    }

    editProfile() {
        this.profileService.editProfile(this.params);
        this.profileService.userUpdated.subscribe((response: any) => {
            if(response == true) {
                this.editPro = 'd-none';
                this.displayPro = '';
                this.show = '';
                this.message = 'Profile Updated';
                let that = this;
                setTimeout(function () {
                    that.show = 'd-none';
                }, 6000)
            }
            
        });
    }

    onChange() {
        if (this.newPassword != this.confirmPassword) {
            this.show = '';
            this.message = 'Passwords do not match';
            let that = this;
            setTimeout(function () {
                that.show = 'd-none';
            }, 6000)
        }
        else if(this.newPassword == this.confirmPassword) {
            this.show = 'd-none';
        }
    }

    changePassword() {
        if(this.oldPassword == '') {
            this.show = '';
            this.message = 'Please enter your old password';
            let that = this;
            setTimeout(function () {
                that.show = 'd-none';
            }, 6000)
        }
        else if(this.newPassword == '') {
            this.show = '';
            this.message = 'Please enter your new password';
            let that = this;
            setTimeout(function () {
                that.show = 'd-none';
            }, 6000)
        }
        else if(this.confirmPassword == '') {
            this.show = '';
            this.message = 'Please confirm your new password';
            let that = this;
            setTimeout(function () {
                that.show = 'd-none';
            }, 6000)
        }
        else if(this.confirmPassword == this.newPassword) {
            let param = {
                email: this.user.email,
                password: this.oldPassword, 
                new_password: this.newPassword
            }

            this.profileService.changePassword(param);
            this.profileService.passwordChanged.subscribe((response:any)=>{
                if(response == true) {
                    this.show = '';
                    this.message = 'Password Changed Successfully';
                    let that = this;
                    setTimeout(function () {
                        that.show = 'd-none';
                    }, 6000)
                }
            });
        }
        else {
            this.show = '';
            this.message = 'Passwords do not match';
            let that = this;
            setTimeout(function () {
                that.show = 'd-none';
            }, 6000)
        }
    }

}