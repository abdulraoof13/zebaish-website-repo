import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, Subject } from "rxjs";
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';
import { of } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from "@angular/core";
import { AuthService } from '../auth/auth.service';

@Injectable({ providedIn: 'root' })
export class ProfileService implements Resolve<any> {
    apiUrl: any = environment.apiUrl;
    gotOrder: BehaviorSubject<any> = new BehaviorSubject({});
    reviewSent: BehaviorSubject<any> = new BehaviorSubject({});
    passwordChanged: BehaviorSubject<any> = new BehaviorSubject({});
    userUpdated: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private router: Router, private authService: AuthService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        return new Promise((resolve, reject) => {

            Promise.all([
                //this.getDress()
            ]).then(
                ([classes]) => {
                    resolve(Response);
                },
                reject
            );
        });
    }

    getOrders(): Promise<any> {

        return new Promise((resolve, reject) => {
            let token = localStorage.getItem('token');
            let user = this.authService.getUserObj();
            const headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            })

            this.http.get(`${this.apiUrl}/api/orders/get_orders_by_customer/${user._id}`, { headers: headers }).subscribe((response: any) => {
                debugger;
                if (response) {
                    this.gotOrder.next(response);
                }
            });
        });
    }

    editProfile(obj:any): Promise<any> {
        return new Promise((resolve, reject) => {
            let token = localStorage.getItem('token');
            const headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            })

            this.http.put(`${this.apiUrl}/api/user`,obj, { headers: headers }).subscribe((response: any) => {
                debugger;
                if (response.message == "Update successful!") {
                    this.authService.getCurrentUser();
                    this.userUpdated.next(true);

                }
            });
        });
    }

    sendReview(obj:any) {
        let token = localStorage.getItem('token');
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        })

        this.http.post(`${this.apiUrl}/api/reviews`,obj, { headers: headers }).subscribe((response: any) => {
            debugger;
            if (response.message=="Review added successfully!") {
                this.reviewSent.next(true);
            }
        });
    }

    changePassword(obj:any) {
        let token = localStorage.getItem('token');
        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        })

        this.http.put(`${this.apiUrl}/api/user/reset_password`,obj, { headers: headers }).subscribe((response: any) => {
            debugger;
            if (response) {
                this.passwordChanged.next(true);
            }
        });
    }


}