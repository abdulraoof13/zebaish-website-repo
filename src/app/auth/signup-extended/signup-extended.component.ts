import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from "@angular/forms";
import { AuthService } from "../auth.service";

@Component({
  selector: 'app-signup-extended',
  templateUrl: './signup-extended.component.html',
  styleUrls: ['./signup-extended.component.scss']
})
export class SignupExtendedComponent implements OnInit {

  public extendedForm: any;
  isLoading = false;

  constructor(public route: ActivatedRoute, public authService: AuthService) { }

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    this.extendedForm = id;

    
  }

  onIndividualExtended(form: NgForm) {
    if (form.invalid) {
    
      return;
    }

       

    this.isLoading = true;
    this.authService.individualExtend(form.value);

  }

  onCompanyExtended(form: NgForm) {
    if (form.invalid) {
    
      return;
    }

       

    this.isLoading = true;
    this.authService.companyExtend(form.value);

  }

}
