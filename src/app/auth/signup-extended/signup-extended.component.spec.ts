import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupExtendedComponent } from './signup-extended.component';

describe('SignupExtendedComponent', () => {
  let component: SignupExtendedComponent;
  let fixture: ComponentFixture<SignupExtendedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignupExtendedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupExtendedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
