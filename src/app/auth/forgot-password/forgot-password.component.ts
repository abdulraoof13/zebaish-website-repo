import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from "@angular/forms";
import { AuthService } from "../auth.service";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotComponent implements OnInit {

  isLoading = false;
  countries : any=[];

  constructor(public route: ActivatedRoute, public authService: AuthService) { }

  ngOnInit(): void {
    this.authService.getCountries();
    this.authService.onCountryChanged.subscribe((response: any) => {
      this.countries = response;
    });

    
  }

  phoneNumber(form: NgForm) {

    let dialingCode: any;

    for(let i=0; i<this.countries.length; i++)
    {
      if (this.countries[i].id == form.value.country)
      {
        dialingCode = this.countries[i].dialing_code;
        break;
      }
    }

    if (form.value.phone[0] == '0') {
      form.value.phone = form.value.phone.slice(1,);
      form.value.phone = dialingCode + form.value.phone;
      debugger;
    }
    else if(form.value.phone[0] == '+') {


    }
    else {
      form.value.phone = dialingCode + form.value.phone;
    }
  }

  onForgotPassword(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.phoneNumber(form);
    this.isLoading = true;
    this.authService.resetPassword(form.value);
  }

  

}
