import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { AuthService } from "../auth.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isLoading: Boolean = false;
  countries : any=[];
  constructor(public authService: AuthService) {}

  ngOnInit(){
    this.authService.onCountryChanged.subscribe((response: any) => {
      this.countries = response;
         
    });
    

  }

  phoneNumber(form: NgForm) {

    let dialingCode: any;

    for(let i=0; i<this.countries.length; i++)
    {
      if (this.countries[i].id == form.value.country)
      {
        dialingCode = this.countries[i].dialing_code;
        break;
      }
    }

    if (form.value.phone[0] == '0') {
      form.value.phone = form.value.phone.slice(1,);
      form.value.phone = dialingCode + form.value.phone;
      debugger;
    }
    else if(form.value.phone[0] == '+') {


    }
    else {
      form.value.phone = dialingCode + form.value.phone;
    }
  }
  validateEmail(form: NgForm) {
    const regularExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regularExpression.test(String(form.value.phone).toLowerCase());
   }

  onLogin(form: NgForm) {
    
    if(this.validateEmail(form)) {
      this.authService.loading = true;
      
      this.authService.login(form.value.phone);
      this.isLoading = this.authService.loading;
    }
    else {
      this.phoneNumber(form);
      this.authService.login(form.value.phone);
      this.isLoading = this.authService.loading;
    }
    
  }
}