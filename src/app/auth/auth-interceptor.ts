import {HttpInterceptor, HttpRequest, HttpHandler} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from "./auth.service";
import { environment } from '../../environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  apiUrl: any = environment.apiUrl;
  constructor(private authService: AuthService) {}
 
  intercept(req: HttpRequest<any>, next: HttpHandler) {
   
     
    if (req.url == `${this.apiUrl}/api/v1/countries`){
        
      const authRequest = req.clone({
    
        headers: req.headers.set("Authorization", '')
      });
      
      return next.handle(authRequest);
    }
    else if (req.url == `${this.apiUrl}/api/v1/users`){
         
      const authRequest = req.clone({
    
        headers: req.headers.set("Authorization", '')
      });
      
      return next.handle(authRequest);
    }
    else if  (req.url == `${this.apiUrl}/api/v1/users/sign_in`){
        
      const authRequest = req.clone({
    
        headers: req.headers.set("Authorization", '')
      });
      
      return next.handle(authRequest);
    }
    else if(req.url == "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=40.6655101,-73.89188969999998&destinations=40.6905615%2C-73.9976592%7C40.6905615%2C-73.9976592%7C40.6905615%2C-73.9976592%7C40.6905615%2C-73.9976592%7C40.6905615%2C-73.9976592%7C40.6905615%2C-73.9976592%7C40.659569%2C-73.933783%7C40.729029%2C-73.851524%7C40.6860072%2C-73.6334271%7C40.598566%2C-73.7527626%7C40.659569%2C-73.933783%7C40.729029%2C-73.851524%7C40.6860072%2C-73.6334271%7C40.598566%2C-73.7527626&key=AIzaSyB1HWmKW_bjT7fYjh1wzVHmZ9C5JQFEaLw"){
        debugger;
      const authRequest = req.clone({
       
      });
      
      return next.handle(authRequest);
    }
    else{
        
      const authToken = localStorage.getItem("token");
      const authRequest = req.clone({
    
        headers: req.headers.set("Authorization", `Bearer ${authToken}`  || "")
      });
      return next.handle(authRequest);  
    }
    
  }
}
