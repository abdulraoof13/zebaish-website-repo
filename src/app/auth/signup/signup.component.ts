import { Component, OnInit, OnDestroy, } from "@angular/core";
import { NgForm } from "@angular/forms";
import { AuthService } from "../auth.service";


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  isLoading = false;
  headers:any;
  countries: any=[];
  constructor(public authService: AuthService) {
    

    // this.authService.getCountries();
  }
  ngOnInit(){
    this.authService.getCountries();
    this.authService.onCountryChanged.subscribe((response: any) => {
      this.countries = response;
         
    });
    

  }

  phoneNumber(form: NgForm) {
    if (form.value.phone[0] == '0') {
      form.value.phone = form.value.phone.slice(1,);
      let phoneNumber = form.value.country + form.value.phone;
      form.value.phone = phoneNumber;
      debugger;
    }
    else if(form.value.phone[0] == '+') {


    }
    else {
      let phoneNumber = form.value.country + form.value.phone;
      form.value.phone = phoneNumber;
    }
  }

  onSignup(form: NgForm) {
    if (form.invalid) {
    
      return;
    }

    this.phoneNumber(form);
    this.isLoading = true;
    this.authService.createUser(form.value);


  }
  
}
