import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { AuthService } from "../auth.service";


@Component({
  selector: 'app-phone-verify',
  templateUrl: './phone-verify.component.html',
  styleUrls: ['./phone-verify.component.scss']
})
export class VerifyComponent implements OnInit {
  isLoading = false;
  countries : any=[];
  constructor(public authService: AuthService) {}

  ngOnInit(){
    //this.authService.getUser();
    this.authService.onCountryChanged.subscribe((response: any) => {
      this.countries = response;
         
    });
    

     }


  onSubmit(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.isLoading = true;
    //this.authService.send_verification_code(form.value.code);
  }
}