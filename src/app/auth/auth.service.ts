import { Injectable, NgZone } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, Subject } from "rxjs";
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';
import { of } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({ providedIn: "root" })
export class AuthService implements Resolve<any> {
  apiUrl: any = environment.apiUrl;
  private isAuthenticated = false;
  private token: any;
  private tokenTimer: any;
  private ringer = false;
  headers: any;
  countries: any = [];
  verification_code: any;
  private authStatusListener = new Subject<boolean>();
  onauthStatusChanged: BehaviorSubject<any> = new BehaviorSubject({});
  onSignup: BehaviorSubject<any> = new BehaviorSubject({});
  onCountryChanged: BehaviorSubject<any> = new BehaviorSubject({});
  onVerificationChanged: BehaviorSubject<any> = new BehaviorSubject({});
  mesg: BehaviorSubject<any> = new BehaviorSubject({});
  user: any;
  cargo: any;
  loading: Boolean = false;
  stringMesg: any = ''
  constructor(private http: HttpClient, private router: Router, public ngzone: NgZone) {

  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    return new Promise((resolve, reject) => {

      Promise.all([
        this.setIsAuth()
      ]).then(
        ([classes]) => {
          resolve(Response);
        },
        reject
      );
    });
  }

  getUserObj() {
    return JSON.parse(localStorage.getItem('user') || '{}');
  }

  getToken() {
    return this.token;
  }
  setIsAuth() {
    let a = localStorage.getItem("token");
    //console.log(a);
    if (a) {
      this.isAuthenticated = true;
      this.onauthStatusChanged.next(true)
    }
    else {
      this.isAuthenticated = false;
    }
  }

  setUser(user: any) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  getIsAuth() {
    this.setIsAuth();
    return this.isAuthenticated;
  }

  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }

  createUser(obj: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.apiUrl}/api/user/signup`, obj, { observe: "response" }).subscribe((response: any) => {
        debugger;
        if (response.status == 201) {
          this.onSignup.next(true)
          //this.router.navigate(['/login']);
        }
        else {
          this.loading = false;
          this.onSignup.next(false)
          this.stringMesg = 'An Account Already exists with same email, phone no. or CNIC';
        }
      })
    }).catch((err) =>
      this.handleError(err, () => this.http.post<{ token: string; user: any }>(`${this.apiUrl}/api/user/login`, obj, { observe: 'response' }))
    );;

  }

  login(obj: any): Promise<any> {
    this.loading = true;
    return new Promise((resolve, reject) => {
      this.http.post(`${this.apiUrl}/api/user/login`, obj, { observe: "response" }).subscribe((response: any) => {
        debugger;
        if (response.status == 200) {
          this.saveAuthData(response.body.token, response.body.userId);
          this.token = response.body.token;
          let cart = 0;
          localStorage.setItem('cartCount', cart.toString());
          this.getCurrentUser();
          this.setIsAuth();
          this.router.navigate(['/']);
        }
        else {
          this.loading = false;
          this.onauthStatusChanged.next(false)
          this.stringMesg = 'Please Enter Valid Login Credentials';
        }
      }, reject);
    }).catch((err) =>
      this.handleError(err, () => this.http.post<{ token: string; user: any }>(`${this.apiUrl}/api/user/login`, obj, { observe: 'response' }))
    );

  }

  protected handleError(error: any, continuation: () => Observable<any>) {
    let that = this;
    if (error.status == 404 || error.status == 400) {
      return of(false);
    } 
    else if (error.status == 401) {
      let sMesg = 'Invalid Credentials';
      this.mesg.next(true);
      this.loading = false;
    } 
    else if (error.status == 401 && error.error.error == 'Invalid Phone no / password') {

      this.loading = false;
    } 
    else if (error.error.error == 'Phone no is not verified') {
      this.router.navigate(['/verify']);
      return of(false);
    } 
    else if (error.status == 422) {
      return of(false);
    }
    else if (error.status == 500) {
      this.onSignup.next(false);
      return of(false);
    }

    return of(false);
  }


  getCurrentUser() {
    let id = localStorage.getItem('userID')
    let token = localStorage.getItem('token')
    debugger;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    })
    this.http.get(`${this.apiUrl}/api/user/${id}`, { headers: headers }).subscribe((response: any) => {
      debugger;
      if (response) {
        this.setUser(response.user)
      }
    });
  }



  autoAuthUser() {
    const authInformation = this.getAuthData();
    if (!authInformation) {
      return;
    }
    const now = new Date();

  }

  logout() {
    this.token = null;
    this.isAuthenticated = false;
    this.authStatusListener.next(false);
    this.onauthStatusChanged.next(false);
    clearTimeout(this.tokenTimer);
    //this.clearAuthData();
    localStorage.clear();
    this.router.navigate(['/']);
  }

  private setAuthTimer(duration: number) {
    console.log("Setting timer: " + duration);
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }

  private saveAuthData(token: string, userID: any) {
    localStorage.setItem("token", token);
    localStorage.setItem("userID", userID);
  }

  private clearAuthData() {
    localStorage.removeItem("token");
    localStorage.removeItem("user");
  }

  private getAuthData() {
    const token = localStorage.getItem("token");
    console.log(token);
    if (!token) {
      return;
    }
    return {
      token: token
    }
  }




}
