import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, Subject } from "rxjs";
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';
import { of } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from "@angular/core";
import { AuthService } from '../auth/auth.service';

@Injectable({ providedIn: 'root' })
export class ContactService implements Resolve<any> {
    apiUrl: any = environment.apiUrl;
    emailed: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private router: Router, private authService: AuthService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        return new Promise((resolve, reject) => {

            Promise.all([
                //this.getDress()
            ]).then(
                ([classes]) => {
                    resolve(Response);
                },
                reject
            );
        });
    }

    saveEmail(obj:any): Promise<any> {

        return new Promise((resolve, reject) => {
            let token = localStorage.getItem('token');
            let user = this.authService.getUserObj();
            const headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            })

            this.http.post(`${this.apiUrl}/api/contact_us`,{content:obj}, { headers: headers }).subscribe((response: any) => {
                debugger;
                if (response) {
                    this.emailed.next(response);
                }
            });
        });
    }


}