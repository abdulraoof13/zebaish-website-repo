import { Component, OnInit } from "@angular/core";
import { ContactService } from "./contact.service";
import { DressService } from "./dresses.service";
import { CategoryService } from "../categories/categories.services";
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { QuickAddComponent } from "../categories/quick-add/quick-add.component";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  email:any='';
  clothes1:any='';
  clothes2:any='';
  dialogRef: any;
  show: any = 'd-none';
  message: any = '';
  isLoading1: Boolean = false;
  isLoading2: Boolean = false;

  constructor(private conatctService: ContactService, private categoryService:CategoryService, public dialog: MatDialog,
    private dressService: DressService,private router: Router ) {}
  

  ngOnInit():void  {

    this.isLoading1=true;
    this.dressService.getDresses(1);
    this.dressService.getDresses(3);
    this.dressService.clothesChanged.subscribe((response:any)=>{
      this.clothes1 = response;
      if (this.clothes1.length > 0) {
        this.isLoading1 = false;
      }
      else if (this.clothes1.length == 0) {
        this.isLoading1 = false;
      }
    });

    this.dressService.clothesChanged2.subscribe((response:any)=>{
      this.clothes2 = response;
      if (this.clothes2.length > 0) {
        this.isLoading2 = false;
      }
      else if (this.clothes2.length == 0) {
        this.isLoading2 = false;
      }
    });
  }

  openDialog(event: any) {
    this.dialogRef = this.dialog.open(QuickAddComponent, {
      panelClass: 'event-form-dialog',
      data: {
        dressData: event
      }
    });
    this.dialogRef.afterClosed().subscribe((response: any) => {
      this.categoryService.addedToCart.subscribe((response: any) => {
        if (response == true) {
          this.show = '';
          this.message = 'Dress added to cart';
          let that = this;
          setTimeout(function () {
            that.show = 'd-none';
          }, 6000)
        }
      });
    });
  }

  mailingList() {
    this.conatctService.saveEmail(this.email);
  }

  navigate(link: any) {
    this.router.navigate(['/dress/' + link]);
  }
}