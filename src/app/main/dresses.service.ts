import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, Subject } from "rxjs";
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';
import { of } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from "@angular/core";
import { AuthService } from '../auth/auth.service'

@Injectable({ providedIn: 'root' })
export class DressService implements Resolve<any> {
    apiUrl: any = environment.apiUrl;

    clothesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    clothesChanged2: BehaviorSubject<any> = new BehaviorSubject({});
    addedToCart: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(private http: HttpClient, private router: Router, private authService: AuthService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        return new Promise((resolve, reject) => {

            Promise.all([
                //this.getDresses()
            ]).then(
                ([classes]) => {
                    resolve(Response);
                },
                reject
            );
        });
    }

    getDresses(id:any): Promise<any> {
        let token = localStorage.getItem('token')
        return new Promise((resolve, reject) => {
            const headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            })
            this.http.get(`${this.apiUrl}/api/clothes?category_id=${id}`, { headers: headers }).subscribe((response:any)=>{
                if(response.clothes && response.maxClothes>0) {
                    if(id==1) {
                        this.clothesChanged.next(response.clothes);
                    }
                    else {
                        this.clothesChanged2.next(response.clothes);
                    }
                    
                }
                console.log(response);
            });
        });

    }

    getFilteredDresses(id:any, filters:any): Promise<any> {
        let token = localStorage.getItem('token')
        let queryString='';
        return new Promise((resolve, reject) => {
            const headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            })
            debugger;
            if(filters.shoulder != '') {
                queryString = queryString +'&'+'shoulder='+filters.shoulder.toString()
            }
            if(filters.waist != '') {
                queryString = queryString +'&'+'waist='+filters.waist.toString()
            }
            if(filters.neck != '') {
                queryString = queryString +'&'+'neck='+filters.neck.toString()
            }
            if(filters.sleeves != '') {
                queryString = queryString +'&'+'sleeves='+filters.sleeves.toString()
            }
            if(filters.size != '') {
                queryString = queryString +'&'+'size='+filters.size;
            }
            if(filters.status != '') {
                queryString = queryString +'&'+'status='+filters.status.toString();
            }
            console.log(queryString);

            this.http.get(`${this.apiUrl}/api/clothes/get_filtered_clothes?category=${id}${queryString}`, { headers: headers }).subscribe((response:any)=>{
                if(response.clothes) {
                    this.clothesChanged.next(response.clothes);
                }
                console.log(response);
            });
        });

    }
    
}