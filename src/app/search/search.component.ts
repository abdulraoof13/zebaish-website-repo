import { Component, OnInit } from "@angular/core";
import { SearchService } from "./search.services";
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { QuickAddComponent } from "../categories/quick-add/quick-add.component";
import { FilterComponent } from "../categories/filter/filter.component";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-seach',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {
  clothes: any;
  dialogRef: any;
  id: any;
  isLoading: any = false;
  show: any = 'd-none';
  message: any = '';
  empty:any = 'd-none'

  constructor(private searchService: SearchService, public dialog: MatDialog, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.isLoading = true;
    //this.id = this.route.snapshot.paramMap.get('id');
    //this.searchService.getDresses(this.id);
    this.searchService.clothesChanged.subscribe((response: any) => {
      this.isLoading = true;
      this.clothes = response;
      if (this.clothes.length > 0) {
        this.isLoading = false;
        this.empty = 'd-none';
      }
      else if (this.clothes.length == 0) {
        this.isLoading = false;
        this.empty = 'empty';
      }
      console.log(response);
    });

    
  }

  addToCart(obj: any) {
    let cart = localStorage.getItem('cartCount')
    let cartCount = parseInt(cart || '') + 1;
    localStorage.setItem('cartCount', cartCount.toString());
    localStorage.setItem('dress' + cartCount.toString(), obj);
  }

  openDialog(event: any) {
    this.dialogRef = this.dialog.open(QuickAddComponent, {
      panelClass: 'event-form-dialog',
      data: {
        dressData: event
      }
    });
    this.dialogRef.afterClosed().subscribe((response: any) => {
      this.searchService.addedToCart.subscribe((response: any) => {
        if (response == true) {
          this.show = '';
          this.message = 'Dress added to cart';
          let that = this;
          setTimeout(function () {
            that.show = 'd-none';
          }, 6000)
        }
      });
    });
  }

  openFilter(event: any) {
    this.dialogRef = this.dialog.open(FilterComponent, {
      panelClass: 'event-form-dialog',
      data: {
        dressData: this.id
      }
    });
    this.dialogRef.afterClosed().subscribe((response: any) => {

    });
  }

  navigate(link: any) {
    this.router.navigate(['/dress/' + link]);
  }



}