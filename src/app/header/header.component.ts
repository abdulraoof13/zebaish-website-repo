import { Component, OnInit } from "@angular/core";
import { AuthService } from '../auth/auth.service';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Router } from '@angular/router';
import { SearchService } from '../search/search.services'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  activate:any ="";
  isAuthenticated:Boolean = false;
  cartCounter:any;
  searchInput:any='';

  constructor(private authService:AuthService, private router: Router,private searchService:SearchService) {}

  ngOnInit(): void {
    this.setIntrvl();
    this.isAuthenticated = this.authService.getIsAuth();
    this.authService.onauthStatusChanged.subscribe((response:any)=>{
      if(response==true) {
        this.isAuthenticated = true;
      }
      else {
        this.isAuthenticated = false;
      }
    });
  }

  search() {
    this.closeNav();
    this.searchService.getDresses(this.searchInput);
  }

  getCount() {
    this.cartCounter = localStorage.getItem('cartCount');
  }

  setIntrvl(){
    setInterval(() => this.getCount(),1000);
  }

  openNav() {
    this.activate = "navigate"
  }
  closeNav() {
    this.activate = ""
  }

  logout() {
    this.closeNav();
    this.authService.logout();
    
  }

  profile() {

  }
}