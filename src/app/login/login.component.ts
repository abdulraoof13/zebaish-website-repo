import { Component } from "@angular/core";
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  params= { 
    email:'',
    password:'' 
  };
  isLoading:String = '';
  message:any="";
  show:any="d-none";

  constructor(private auth:AuthService){}



  validateEmail(email:any) {
    const regularExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regularExpression.test(String(email).toLowerCase());
  }

  submitLogin() {
    this.show='d-none';
    this.isLoading= 'loading'
    if(this.validateEmail(this.params.email)) {
      debugger;
      this.auth.login(this.params).then((response:any)=>{
        console.log(response)
      });
      this.auth.mesg.subscribe((res:any)=>{
        if(res==true) {
          this.isLoading = '';
          this.message='Invalid Credentials';
          this.show="";
          console.log(res);
        }
       
      });
      
    }
    else {
      this.show="";
      this.message = 'Please Enter A Valid Email Address'
    }
    
  }

  


  
  
}