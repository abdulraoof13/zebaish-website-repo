import { Component, Inject, ViewEncapsulation, AfterViewChecked } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CategoryService } from '../categories.services';

@Component({
    selector: 'filter-dialog',
    templateUrl: './filter.component.html',
    styleUrls: ['./filter.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class FilterComponent {

    days:any='';
    warning:Boolean = false;
    radioSelected:any;
    waist:any='';
    shoulders:any='';
    neck:any='';
    sleeves:any='';
    size:any='';
    status:any='';
    prize:any;
    Sclicked:any='';
    Mclicked:any='';
    Lclicked:any='';
    XLclicked:any='';
    filter:any;

    constructor(public dialogRef: MatDialogRef<FilterComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
    private router:Router, private categoryService: CategoryService) {
        this.data;
    }

    ngOnInit()
    {
        
        
        console.log(this.data);

    }

    addToCart(obj:any) {
        if(this.days=='') {
            this.warning=true;
            return;
        }
        obj.orderedDays = this.days;
        let cart = localStorage.getItem('cartCount')
        let cartCount = parseInt(cart||'') + 1;
        localStorage.setItem('cartCount',cartCount.toString());
        localStorage.setItem('dress'+cartCount.toString(),JSON.stringify(obj));
    }

    closeDialog(action:any) {
        if(action=='cancel'){
            this.dialogRef.close();
        }   
    }

    applyFilters() {
        this.filter = {
            shoulder: this.shoulders,
            neck: this.neck,
            waist: this.waist,
            sleeves: this.sleeves,
            size: this.size,
            status:this.status
        }
        this.categoryService.getFilteredDresses(this.data.dressData,this.filter);

        this.closeDialog('cancel');
    }

    checkOut() {
        console.log(this.radioSelected);
        localStorage.setItem('toOrder',JSON.stringify(this.radioSelected));
        this.router.navigate(['/order']);
        this.closeDialog('cancel');
    }

    selectSize(size:any) {
        this.size = size;
        if(size=='Small') {
            this.Sclicked = 'clicked';
            this.Mclicked = '';
            this.Lclicked = '';
            this.XLclicked = '';
        }
        else if(size=='Medium') {
            this.Sclicked = '';
            this.Mclicked = 'clicked';
            this.Lclicked = '';
            this.XLclicked = '';
        }
        else if(size=='Large') {
            this.Sclicked = '';
            this.Mclicked = '';
            this.Lclicked = 'clicked';
            this.XLclicked = '';
        }
        else if(size=='Extra-Large') {
            this.Sclicked = '';
            this.Mclicked = '';
            this.Lclicked = '';
            this.XLclicked = 'clicked';
        }
    }

}