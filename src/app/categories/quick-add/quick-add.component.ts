import { Component, Inject, ViewEncapsulation, AfterViewChecked } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { CategoryService } from '../categories.services';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
    selector: 'quick-dialog',
    templateUrl: './quick-add.component.html',
    styleUrls: ['./quick-add.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class QuickAddComponent {

    days:any='';
    warning:Boolean = false;
    authenticated = false;

    constructor(public dialogRef: MatDialogRef<QuickAddComponent>, @Inject(MAT_DIALOG_DATA) public data: any, 
    private categoryService: CategoryService, private authService: AuthService) {
        this.data;
    }

    ngOnInit()
    {
        if(this.authService.getIsAuth()) {
            this.authenticated = true;
        }
        console.log(this.data);

    }

    addToCart(obj:any) {
        if(this.days=='') {
            this.warning=true;
            return;
        }
        obj.orderedDays = this.days;
        let cart = localStorage.getItem('cartCount')
        let cartCount = parseInt(cart||'') + 1;
        localStorage.setItem('cartCount',cartCount.toString());
        localStorage.setItem('dress'+cartCount.toString(),JSON.stringify(obj));
        this.categoryService.addedToCart.next(true);
        this.closeDialog('cancel');
    }

    closeDialog(action:any) {
        if(action=='cancel'){
            this.dialogRef.close();
        }   
    }

}