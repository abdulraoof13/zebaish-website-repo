import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { FooterComponent } from './footer/footer.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { CategoriesComponent } from './categories/categories.component';
import { DressComponent } from './dress/dress.composnent';
import { MatProgressBarModule} from '@angular/material/progress-bar';
import { MatRadioModule} from '@angular/material/radio';
import { DressService } from './dress/dress.services';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CartComponent } from './cart/cart.component';
import { OrderComponent } from './order/order.component';
import { ProfileSettingComponent } from './profile-setting/profile-setting.component';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { QuickAddComponent } from './categories/quick-add/quick-add.component';
import { ProceedComponent } from './cart/proceed/proceed.component';
import { ConfirmComponent } from './order/confirm/confirm.component';
import { MesgComponent } from './thank-you/mesg.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ReviewComponent } from './profile-setting/review/review.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-passowrd.component';
import { FilterComponent } from './categories/filter/filter.component';
import { HelpComponent } from './help/help.component';
import { SearchComponent } from './search/search.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    FooterComponent,
    SignupComponent,
    LoginComponent,
    CategoriesComponent,
    DressComponent,
    CartComponent,
    OrderComponent,
    ProfileSettingComponent,
    QuickAddComponent,
    ProceedComponent,
    ConfirmComponent,
    MesgComponent,
    ReviewComponent,
    ForgotPasswordComponent,
    FilterComponent,
    HelpComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatProgressBarModule,
    MatRadioModule,
    MatDialogModule,
    MatProgressSpinnerModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [QuickAddComponent, ProceedComponent, ConfirmComponent, ReviewComponent, FilterComponent]
})
export class AppModule { }
