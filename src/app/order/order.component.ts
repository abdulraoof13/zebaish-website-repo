import { Component, OnInit } from "@angular/core";
import { OrderService } from "./order.service";
import { AuthService } from '../auth/auth.service'
import { DressService } from "../dress/dress.services";
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ConfirmComponent } from "./confirm/confirm.component";
import { Router } from '@angular/router';


@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss']
})
export class OrderComponent {
    dress: any;
    today = new Date();
    totalRent:any=0;
    security:any = 0;
    delivery:any='';
    payment:any='';
    address:any='';
    message:any='';
    show:any = 'd-none';
    agree:any='';
    dialogRef:any
    cartCount: any;
    clothes: any=[];
    dressNames: any=[];
    user: any;
    vendor:any;
    processing:any=false;

    constructor(private dressService: DressService, private orderService: OrderService, private authService: AuthService, public dialog: MatDialog, private router: Router) { }

    ngOnInit(): void {
        this.user = JSON.parse(localStorage.getItem('user') || '');
        this.cartCount = localStorage.getItem('cartCount');
        for (let i = 0; i < parseInt(this.cartCount || '0'); i++) {
            this.clothes.push(JSON.parse(localStorage.getItem('dress' + (i + 1).toString()) || '{}'))
            this.dressNames.push('dress' + (i + 1).toString()); 
        }

        console.log(this.clothes)

        for(let i=0;i<this.clothes.length;i++) {
            this.totalRent = this.clothes[i].basic_rent * this.clothes[i].orderedDays + this.totalRent;
            let temp = (this.clothes[i].basic_rent * this.clothes[i].orderedDays)/2
            this.security = this.security + temp;
        }
        //this.dress = JSON.parse(localStorage.getItem('toOrder')||'');
        
    }

    confirm() {
        this.processing=true;
        this.show = 'd-none';
        debugger;
        if(this.delivery == '') {
            this.message='Please Select a Delivery Method';
            this.show='';
        } 
        else if(this.payment == '') {
            this.message='Please Select a Payment Method';
            this.show='';
        }
        else if(this.address=='') {
            this.message='Please Provide an Address';
            this.show='';
        }
        else if(this.agree=='') {
            this.message='You must agree to Terms and Conditions, to rent a dress';
            this.show='';
        }
        else  {

            for(let i=0;i<this.clothes.length;i++) {
                let today = new Date();
                let start = new Date();
                let end = new Date();
                start.setDate(today.getDate()+1);
                end.setDate(start.getDate()+this.clothes[i].orderedDays);
                let order = {
                    start_date: start.toISOString(),
                    end_date:end.toISOString(),
                    address:this.address,
                    deliveryMethod:this.delivery,
                    paymentMethod:this.payment,
                    rent: (this.clothes[i].basic_rent * this.clothes[i].orderedDays)+((this.clothes[i].basic_rent * this.clothes[i].orderedDays)/2)+100,
                    simple_rent:this.clothes[i].basic_rent * this.clothes[i].orderedDays,
                    number_of_days:this.clothes[i].orderedDays,
                    delivery_charges:100,
                    security_deposit:(this.clothes[i].basic_rent * this.clothes[i].orderedDays)/2,
                    delivery_address: this.address,
                    order_status: 1,
                    cloth_id: this.clothes[i]._id,
                    vendor_id: this.clothes[i].vendor_id,
                    customer_id: this.user._id
                }
                if(this.clothes[i].vendor_id._id) {
                    order.vendor_id = this.clothes[i].vendor_id._id;
                }
                else {
                    order.vendor_id = this.clothes[i].vendor_id;
                }
                console.log(order)
                this.orderService.orderDress(order);
            }
            

            //this.openDialog(order);
            this.orderService.gotOrder.subscribe((response:any)=>{
                if(response == true) {
                    this.message='Order Placed';
                    this.show='';
                    let that =this;
                    setTimeout(function () {
                        that.show = 'd-none';
                        that.router.navigate(['/thankyou']);
                    }, 3000)
                }
                else if(response == false) {
                    this.message='Something went worng';
                    this.show='';
                    let that =this;
                    this.processing = false;
                    setTimeout(function () {
                        that.show = 'd-none';
                        //that.router.navigate(['/thankyou']);
                    }, 3000)
                }
            });
        }
    }

    openDialog(event: any) {
        this.dialogRef = this.dialog.open(ConfirmComponent, {
            panelClass: 'event-form-dialog',
            data: {
                dressData: event
            }
        });
        this.dialogRef.afterClosed().subscribe((response: any) => {

        });
    }

}