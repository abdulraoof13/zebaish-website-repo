import { Component, Inject, ViewEncapsulation, AfterViewChecked } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { OrderService } from '../order.service';

@Component({
    selector: 'confirm-dialog',
    templateUrl: './confirm.component.html',
    styleUrls: ['./confirm.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ConfirmComponent {

    days: any = '';
    warning: Boolean = false;
    radioSelected: any;
    user: any;
    dress: any;

    constructor(public dialogRef: MatDialogRef<ConfirmComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private router: Router, private orderService: OrderService) {
        this.data;
    }

    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('user') || '');
        this.dress = JSON.parse(localStorage.getItem('toOrder')||'');
    }

    addToCart(obj: any) {
        if (this.days == '') {
            this.warning = true;
            return;
        }
        obj.orderedDays = this.days;
        let cart = localStorage.getItem('cartCount')
        let cartCount = parseInt(cart || '') + 1;
        localStorage.setItem('cartCount', cartCount.toString());
        localStorage.setItem('dress' + cartCount.toString(), JSON.stringify(obj));
    }

    closeDialog(action: any) {
        if (action == 'cancel') {
            this.dialogRef.close();
        }
    }

    checkOut() {
        console.log(this.radioSelected);
        localStorage.setItem('toOrder', JSON.stringify(this.radioSelected));
        this.router.navigate(['/order']);
        this.closeDialog('cancel');
    }

    order() {
        let order = this.data.dressData;
        order.order_status = 1;
        order.cloth_id = this.dress._id;
        order.vendor_id = this.dress.vendor_id;
        order.customer_id = this.user._id;
        console.log(order);
        this.orderService.orderDress(order);
        this.orderService.gotOrder.subscribe((response:any)=>{
            if(response == true) {
                this.router.navigate(['/']);
            }
        });
        
    }

}