import { Component } from "@angular/core";
import { ContactService } from "../main/contact.service";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {

  email:any='';

  constructor(private conatctService: ContactService) {}
  mailingList() {
    this.conatctService.saveEmail(this.email);
  }
}